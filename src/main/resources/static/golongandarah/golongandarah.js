function getAllGolonganDarah(){
	$("#golongandarahTable").html(
		`<thead>
			<tr>
				<th>Kode</th>
				<th>Deskripsi</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="golongandarahTBody"></tbody>
		`
	);

	$.ajax({
		url : "/api/getallgolongandarah",
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
				$("#golongandarahTBody").append(
					`
					<tr>
                        <td>${data[i].code}</td>
                        <td>${data[i].description}</td>
						<td>
							<button value="${data[i].id}" onClick="editGolonganDarah(this.value)" class="btn btn-warning">
								<i class="bi-pencil-square"></i>
							</button>
							<button value="${data[i].id}" onClick="deleteGolonganDarah(this.value)" class="btn btn-danger">
								<i class="bi-trash"></i>
							</button>
						</td>
					</tr>
					`
				)
			}
		}
	});
}

$("#addBtn").click(function(){
	$.ajax({
		url: "/golongandarah/addgolongandarah",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Create New Golongan Darah");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})

function editGolonganDarah(id){
	$.ajax({
		url: "/golongandarah/editgolongandarah/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Edit Golongan Darah");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function deleteGolonganDarah(id){
	$.ajax({
		url: "/golongandarah/deletegolongandarah/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Delete Golongan Darah");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function SearchGolonganDarah(request) {
//	console.log(request)
	if (request.length > 0)
	{
		$.ajax({
			url: '/api/searchgolongandarah/' + request,
			type: 'GET',
			contentType: 'application/json',
			success: function (result) {
//				console.log(result)
				let table = "<table class='table table-stripped mt-3'>";
			    table += "<tr> <th>Kode</th> <th>Deskripsi</th> <th>Action</th>"
				if (result.length > 0)
				{
					for (let i = 0; i < result.length; i++) {
						table += "<tr>";
                        table += "<td>" + result[i].code + "</td>";
                        table += "<td>" + result[i].description + "</td>";
                        table += "<td><button class='btn btn-warning' value='" + result[i].id + "' onclick=editGolonganDarah(this.value)><i class='bi-pencil-square'></i></button> <button class='btn btn-danger' value='" + result[i].id + "' onclick=deleteGolonganDarah(this.value)><i class='bi-trash'></i></button></td>";
						table += "</tr>";
					}
				} else {
					table += "<tr>";
					table += "<td colspan='3' class='text-center'>No data</td>";
					table += "</tr>";
				}
				table += "</table>";
				$('#golongandarahTable').html(table);
			}
		});
	} else {
		getAllGolonganDarah();
	}
}
$(document).ready(function(){
getAllGolonganDarah();
})