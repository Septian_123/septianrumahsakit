function GetAllHubunganPasien(){
	$("#hubunganPasienTable").html(
		`<thead>
			<tr>
				<th width='60%'>Name</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="hubunganPasienTBody"></tbody>
		`
	);

	$.ajax({
		url : "/api/getallhubunganpasien",
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
				$("#hubunganPasienTBody").append(
					`
					<tr>
						<td>${data[i].name}</td>
						<td>
							<button value="${data[i].id}" onClick="editHubunganPasien(this.value)" class="btn btn-warning">
								<i class="bi-pencil-square"></i>
							</button>
							<button value="${data[i].id}" onClick="deleteHubunganPasien(this.value)" class="btn btn-danger">
								<i class="bi-trash"></i>
							</button>
						</td>
					</tr>
					`
				)
			}
		}
	});
}

$("#addBtn").click(function(){
	$.ajax({
		url: "/hubunganpasien/addhubunganpasien",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Create New Hubungan Pasien");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})

function editHubunganPasien(id){
	$.ajax({
		url: "/hubunganpasien/edithubunganpasien/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Edit Hubungan Pasien");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function deleteHubunganPasien(id){
	$.ajax({
		url: "/hubunganpasien/deletehubunganpasien/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Delete Hubungan Pasien");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function SearchHubunganPasien(request) {
	//console.log(request)
	if (request.length > 0)
	{
		$.ajax({
			url: '/api/searchhubunganpasien/' + request,
			type: 'GET',
			contentType: 'application/json',
			success: function (result) {
				//console.log(result)
				let table = "<table class='table table-stripped mt-3'>";
			    table += "<tr> <th width='60%'>Name</th> <th>Action</th>"
				if (result.length > 0)
				{
					for (let i = 0; i < result.length; i++) {
						table += "<tr>";
                        table += "<td>" + result[i].name + "</td>";
                        table += "<td><button class='btn btn-warning' value='" + result[i].id + "' onclick=editHubunganPasien(this.value)><i class='bi-pencil-square'></i></button> <button class='btn btn-danger' value='" + result[i].id + "' onclick=deleteHubunganPasien(this.value)><i class='bi-trash'></i></button></td>";
						table += "</tr>";
					}
				} else {
					table += "<tr>";
					table += "<td colspan='2' class='text-center'>No data</td>";
					table += "</tr>";
				}
				table += "</table>";
				$('#hubunganPasienTable').html(table);
			}
		});
	} else {
		GetAllHubunganPasien();
	}
}
$(document).ready(function(){
GetAllHubunganPasien();
})