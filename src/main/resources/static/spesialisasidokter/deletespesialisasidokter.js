$(document).ready(function() {
	GetSpesialisasiDokterById();

})

var createdOn;
var createdBy;

function GetSpesialisasiDokterById() {
	var id = $("#deleteSpesialisasiDokterId").val();
	$.ajax({
		url: "/api/getbyidspesialisasidokter/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
		    createdBy = data.created_By;
            createdOn = data.created_On;
			$("#idDelete").text(data.name);
		}
	})
}

$("#deleteBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#deleteBtnDelete").click(function() {
	var id = $("#deleteSpesialisasiDokterId").val();
	$.ajax({
		url : "/api/deletespesialisasidokter/" + id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
            $(".modal").modal("hide")
            location.reload();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})