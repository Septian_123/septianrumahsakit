function GetAllBiodataDokter(){
	$("#biodataDokterTable").html(
		`<thead>
			<tr>
				<th>ID</th>
				<th>Name Dokter</th>
				<th>Nomor Str</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="biodataDokterTBody"></tbody>
		`
	);

	$.ajax({
		url : "/api/getallbiodatadokter",
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
				$("#biodataDokterTBody").append(
					`
					<tr>
						<td>${data[i].id}</td>
						<td>${data[i].biodata.fullname}</td>
						<td>${data[i].str}</td>
						<td>
							<button value="${data[i].id}" onClick="infoDokterTreatment(this.value)" class="btn btn-info">
								<i class="bi-info-square"></i>
							</button>
						</td>
					</tr>
					`
				)
			}
		}
	});
}

$("#addBtn").click(function(){
	$.ajax({
		url: "/biodatadokter/addbiodatadokter",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Create New Dokter");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})

$(document).ready(function(){
GetAllBiodataDokter();
})