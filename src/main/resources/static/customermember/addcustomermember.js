$("#addCustomerMemberBtnCancel").click(function(){
	$(".modal").modal("hide")
})

$(document).ready(function(){
    opsiReload();
})

function opsiReload(){
   var jkSel = document.getElementById("opsiJenisKelamin");
   var golDarSel = document.getElementById("opsiGolonganDarah");
   var rhesusSel = document.getElementById("opsiRhesus");
   var relasiSel = document.getElementById("opsiRelasi");

   jkSel.options[jkSel.options.length] = new Option("Pria", "P");
   jkSel.options[jkSel.options.length] = new Option("Wanita", "W");

   rhesusSel.options[rhesusSel.options.length] = new Option("Rh +", "Plus");
   rhesusSel.options[rhesusSel.options.length] = new Option("Rh -", "Minus");

   $.ajax({
       url : "/api/getallgolongandarah",
       type : "GET",
       contentType : "application/json",
       success: function(data){
           for(i = 0; i<data.length; i++){
               golDarSel.options[golDarSel.options.length] = new Option(data[i].code, data[i].id);
           }
       }
   });

   $.ajax({
       url : "/api/getallhubunganpasien",
       type : "GET",
       contentType : "application/json",
       success: function(data){
           for(i = 0; i<data.length; i++){
               relasiSel.options[relasiSel.options.length] = new Option(data[i].name, data[i].id);
           }
       }
   });
}

$("#addCustomerMemberBtnCreate").click(function(){
    var namaMember = $("#namaMemberPasienInput").val();
    var tanggalLahir = $("#dobMember").val();
    var jenisKelamin = $("#opsiJenisKelamin").val();
    var golonganDarah = $("#opsiGolonganDarah").val();
    var rhesus = $("#opsiRhesus").val();
    var tinggiBadan = $("#tinggiBadanInput").val();
    var beratBadan = $("#beratBadanInput").val();
    var relasi = $("#opsiRelasi").val();

    if(namaMember == ""){
    	$("#errNamaMemberPasien").text("Nama tidak boleh kosong!");
    	return;
    } else {
    	$("#errNamaMemberPasien").text("");
    }
    if(tanggalLahir == ""){
    	$("#errDobMember").text("Tanggal Lahir tidak boleh kosong!");
    	return;
    } else {
    	$("#errDobMember").text("");
    }
    if(jenisKelamin == ""){
    	$("#errJenisKelamin").text("Jenis Kelamin tidak boleh kosong!");
    	return;
    } else {
    	$("#errJenisKelamin").text("");
    }
    if(golonganDarah == ""){
    	$("#errGolonganDarah").text("Golongan Darah tidak boleh kosong!");
    	return;
    } else {
    	$("#errGolonganDarah").text("");
    }
    if(rhesus == ""){
    	$("#errRhesus").text("Rhesus tidak boleh kosong!");
    	return;
    } else {
    	$("#errRhesus").text("");
    }
    if(tinggiBadan == ""){
    	$("#errTinggiBadan").text("Tinggi Badan tidak boleh kosong!");
    	return;
    } else {
    	$("#errTinggiBadan").text("");
    }
    if(beratBadan == ""){
    	$("#errBeratBadan").text("Berat Badan tidak boleh kosong!");
    	return;
    } else {
    	$("#errBeratBadan").text("");
    }
    if(relasi == ""){
    	$("#errRelasi").text("Relasi tidak boleh kosong!");
    	return;
    } else {
    	$("#errRelasi").text("");
    }

    var objToBiodata = {};
    objToBiodata.fullname = namaMember;

    var myJsonToBiodata = JSON.stringify(objToBiodata);

    $.ajax({
    	url : "/api/addbiodata",
    	type : "POST",
    	contentType : "application/json",
    	data : myJsonToBiodata,
    	success: function(data){
    	    console.log(data);
    	    var idBiodataBaru = data.id;
    	    var objToCustomer = {};
            objToCustomer.dob = tanggalLahir;
            objToCustomer.gender = jenisKelamin;
            objToCustomer.blood_group_id = golonganDarah;
            objToCustomer.rhesus_Type = rhesus;
            objToCustomer.height = tinggiBadan;
            objToCustomer.weight = beratBadan;
            objToCustomer.biodata_id = idBiodataBaru;

            var myJsonToCustomer = JSON.stringify(objToCustomer);

    	$.ajax({
           url : "/api/addcustomer",
           type : "POST",
           contentType : "application/json",
           data : myJsonToCustomer,
           success: function(data){
               console.log(data);
               var idCustomerBaru = data.id;
               var idParent = localStorage.getItem('idParentDetails');
               var obj = {};
               obj.customer_id = idCustomerBaru;
               obj.customer_relation_id = relasi;
               obj.parent_biodata_id = idParent;

               var myJson = JSON.stringify(obj);

           $.ajax({
               url : "/api/addcustomermember",
               type : "POST",
               contentType : "application/json",
               data : myJson,
               success: function(data){
               console.log(data);
               		$(".modal").modal("hide")
               		location.reload();
               },
               error: function(){
               	alert("Terjadi kesalahan")
               }
               });
           },
           error: function(){
           	alert("Terjadi kesalahan")
           }
           });
    	},
    	error: function(){
    		alert("Terjadi kesalahan")
    	}
    });
})