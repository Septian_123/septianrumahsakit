$(document).ready(function() {
	getEditRole();

})

var createdOn;
var createdBy;

function getEditRole() {
	var id = $("#editRoleId").val();
	$.ajax({
		url: "/api/getrole/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#idEditInput").val(data.id);
			$("#nameEditInput").val(data.name);
			$("#inputKode").val(data.code);
			//taruh data kiri, ambil data kanan
			createdOn = data.created_on;
            createdBy = data.created_by;
            console.log(data);
		}
	});
}

$("#editRoleBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#editRoleBtnCreate").click(function() {
	var id = $("#idEditInput").val();
	var name = $("#nameEditInput").val();
	var code = $("#inputKode").val();

	var obj = {};
	obj.id = id;
	obj.name = name;
	obj.code = code;
	obj.created_on = createdOn;
    obj.created_by =  createdBy;

	var myJson = JSON.stringify(obj);

    console.log(myJson)

	$.ajax({
		url: "/api/editrole/" + id,
		type: "PUT",
		contentType: "application/json",
		data: myJson,
		success: function(data) {
				$(".modal").modal("hide")
				getEditRole();
				location.reload();
		},
		error: function() {
			alert("Terjadi kesalahan")
		}
	});
})