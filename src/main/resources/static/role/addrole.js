$("#addRoleBtnCancel").click(function(){
	$(".modal").modal("hide")
})

$("#addRoleBtnCreate").click(function(){
	var name = $("#inputNama").val();
	var code = $("#inputKode").val();

	if(name == ""){
		$("#errName").text("Nama tidak boleh kosong!");
		return;
	} else {
		$("#errName").text("");
	}

	if(code == ""){
        $("#errKode").text("Kode tidak boleh kosong!");
        return;
    } else {
        $("#errName").text("");
    }

    addRoleName(function(rolename) {
        var isNameExist = rolename.some(function(item) {
            return item.name === name;
        });

        if (isNameExist) {
            $("#errName").text("Nama sudah ada!");
            return;
        } else {
            $("#errName").text("");
        }

        var obj = {};
        obj.name = name;
        obj.code = code;

        var myJson = JSON.stringify(obj);

        $.ajax({
            url : "/api/addrole",
            type : "POST",
            contentType : "application/json",
            data : myJson,
            success: function(data){
                    $(".modal").modal("hide")
                    getTabelKolomRole();
                    location.reload();
            },
            error: function(){
                alert("Terjadi kesalahan")
            }
        });
    });
})

function addRoleName(callback) {
    $.ajax({
        url: "/api/getallrole",
        type: "GET",
        contentType: "application/json",
        success: function(rolename) {
            if (callback && typeof callback === "function") {
                callback(rolename);
            }
        },
        error: function(error) {
            if (callback && typeof callback === "function") {
                callback(error);
            }
        }
    });
}