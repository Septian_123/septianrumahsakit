function GetAllBiodataDokter(){
	$("#profileDokterTable").html(
		`<thead>
			<tr>
				<th>ID</th>
				<th>Name Dokter</th>
				<th>Nomor STR</th>
				<th>Universitas</th>
				<th>Jurusan</th>
				<th>Tahun</th>
				<th>Nama RS</th>
				<th>Spesialis</th>
				<th>Tahun masuk</th>
				<th>Tahun selesai</th>
				<th>Treatment</th>
			</tr>
		</thead>
		<tbody id="profileDokterTBody"></tbody>
		`
	);

	$.ajax({
		url : "/api/getallprofildoktershare",
		type : "GET",
		contentType : "application/json",
		success: function(data){
		console.log(data);
			for(i = 0; i<data.length; i++){
				$("#profileDokterTBody").append(
					`
					<tr>
						<td>${data[i].biodataDokter.id}</td>
						<td>${data[i].biodata.fullname}</td>
						<td>${data[i].biodataDokter.str}</td>
						<td>${data[i].dokterEducation.institution_name}</td>
						<td>${data[i].dokterEducation.major}</td>
						<td>${data[i].dokterEducation.end_year}</td>
						<td>${data[i].doctorOffice.medicalFacility.name}</td>
						<td>${data[i].doctorOffice.specialization}</td>
						<td>${data[i].doctorOffice.start_date}</td>
						<td>${data[i].doctorOffice.end_date}</td>
						<td>
                            <button value="${data[i].biodataDokter.id}" onClick="infotreatment(this.value)" class="btn btn-info">
                                <i class="bi bi-info-square"></i>
                            </button>
                        </td>
					</tr>
					`
				)
			}
		}
	});
}


function infotreatment(id){
    $.ajax({
        url: "/profildoktershare/info/" + id,
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal-title").text("Treatment");
            $(".modal-body").html(data);
            $(".modal").modal("show");
            $(".modal-dialog").addClass("modal-lg");
        }
    });
}


var selectDokter = document.getElementById("pilihDokter");
selectDokter.onchange = function(){
    var id_dokter = selectDokter.value;
    $("#profileDokterTBody").html(``);
    $.ajax({
            url : "/api/getbyidprofildoktershare/" + id_dokter,
            type : "GET",
            contentType : "application/json",
            success: function(data){
            console.log(data);
                for(i = 0; i<data.length; i++){
               $("#profileDokterTBody").append(
               	`
               	        <tr>
               				<td>${(data[i].biodataDokter).id}</td>
               				<td>${(data[i].biodata).fullname}</td>
               				<td>${(data[i].biodataDokter).str}</td>
               				<td>${(data[i].dokterEducation).institution_name}</td>
               				<td>${(data[i].dokterEducation).major}</td>
               				<td>${(data[i].dokterEducation).end_year}</td>
               				<td>${(data[i].doctorOffice).medicalFacility.name}</td>
               				<td>${(data[i].doctorOffice).specialization}</td>
               				<td>${(data[i].doctorOffice).start_date}</td>
               				<td>${(data[i].doctorOffice).end_date}</td>
               				<td>
                                 <button value="${data[i].biodataDokter.id}" onClick="infotreatment(this.value)" class="btn btn-info">
                                      <i class="bi bi-info-square"></i>
                                 </button>
                           </td>
               			</tr>
               			`
               		)
                }
            }
        });

}

function opsiDokter(){
        $.ajax({
            url : "/api/getallprofildoktershare",
            type : "GET",
            contentType : "application/json",
            success: function(data){
            console.log(data);
                for(i = 0; i<data.length; i++){
                $("#pilihDokter").append(
                `<option value="${data[i].biodata.id}">${data[i].biodata.fullname}</option>`
                 )
                }
            }
        });
}



$(document).ready(function(){
GetAllBiodataDokter();
opsiDokter();
console.log("ready");
})