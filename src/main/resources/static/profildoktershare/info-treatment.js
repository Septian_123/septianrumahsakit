$(document).ready(function(){
    getTreatment();
})

function getTreatment(){
    var id = $("#infoTreatmentId").val();
    $.ajax({
        url: "/api/getbyiddoktertreatment/" + id,
        type: "GET",
        contentType: "html",
        success: function(data){
        console.log(data);
            for(i=0 ; i<data.length ; i++){
                 $("#infoTBody").append(
                    `
                    <tr>
                        <td>${data[i].name}</td>
                        <td>
                            <button value="${data[i].id}" onClick="deleteTraetment(this.value)" class="btn btn-danger">
                                  <i class="bi-dash"></i>
                            </button>
                        </td>
                    </tr>
                    `
                );
            }

        }
    });
}

$("#btnTambahTreatment").click(function(){
    var id = $("#infoTreatmentId").val();
	$.ajax({
		url: "/doktertreatment/adddoktertreatment/"+id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Tambah Tindakan");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})

function deleteTraetment(id){
    var id_dokter = $("#infoTreatmentId").val();
	$.ajax({
		url: "/doktertreatment/deletedoktertreatment/iddokter="+ id_dokter+"&idtreatment=" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Hapus Tindakan");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}
