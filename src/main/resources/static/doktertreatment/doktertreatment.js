function GetAllDokterTreatment(){
	$("#doktertreatmentTable").html(
		`<thead>
			<tr>
				<th>ID</th>
				<th>Name Dokter</th>
				<th>Tindakan Dokter</th>
			</tr>
		</thead>
		<tbody id="doktertreatmentTBody"></tbody>
		`
	);

	$.ajax({
		url : "/api/getalldoktertreatment",
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
				$("#doktertreatmentTBody").append(
					`
					<tr>
						<td>${data[i].id}</td>
						<td>${data[i].biodataDokter.biodata.fullname}</td>
						<td>${data[i].name}</td>

					</tr>
					`
				)
			}
		}
	});
}

$(document).ready(function(){
GetAllDokterTreatment();
})