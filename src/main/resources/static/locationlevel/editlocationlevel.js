$(document).ready(function() {
	GetLocationLevelById();
})

var createdOn;
var createdBy;

function GetLocationLevelById() {
	var idLocationLevel = $("#editLocationLevelId").val();
	$.ajax({
		url: "/api/getbyidlocationlevel/" + idLocationLevel,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
		    createdBy = data.created_By;
		    createdOn = data.created_On;
			$("#namaInput").val(data.name);
			$("#namaSingkatInput").val(data.abbreviation);
		}
	})
}

$("#addCancelBtn").click(function() {
	$(".modal").modal("hide")
})

$("#addCreateBtn").click(function() {
	var idLocationLevel = $("#editLocationLevelId").val();
    var nama = $("#namaInput").val();
    var namaSingkat = $("#namaSingkatInput").val();

    if(nama == ""){
        $("#errNama").text("Nama tidak boleh kosong!");
        return;
    }else{
        $("#errNama").text("");
    }
    if(namaSingkat == ""){
        $("#errNamaSingkat").text("Nama tidak boleh kosong!");
        return;
        }else{
            $("#errNamaSingkat").text("");
        }

	var obj = {};
	obj.created_By = createdBy;
	obj.created_On = createdOn;
	obj.id = idLocationLevel;
    obj.name = nama;
    obj.abbreviation = namaSingkat;
	var myJson = JSON.stringify(obj);

//    console.log(myJson);

	$.ajax({
		url: "/api/editlocationlevel/" + idLocationLevel,
		type: "PUT",
		contentType: "application/json",
		data: myJson,
		success: function(data) {
				$(".modal").modal("hide")
				location.reload();
		},
		error: function() {
			alert("Data Yang Anda Masukkan Sudah Ada")
		}
	});
})