package MiniProject.com.RumahSakitTeamA.models;

import javax.persistence.*;

@Entity
@Table(name = "m_wallet_default_nominal")
public class NominalDompetElektronik extends BaseProperties {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "nominal", nullable = true)
        private Integer nominal;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNominal() {
        return nominal;
    }

    public void setNominal(Integer nominal) {
        this.nominal = nominal;
    }
}
