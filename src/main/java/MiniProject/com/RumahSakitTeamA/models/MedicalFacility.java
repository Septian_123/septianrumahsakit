package MiniProject.com.RumahSakitTeamA.models;

import javax.persistence.*;

@Entity
@Table(name = "m_medical_facility")
public class MedicalFacility extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "name", length = 50)
    private String name;

    @ManyToOne
    @JoinColumn(name = "medical_facility_category_id", insertable = false, updatable = false)
    public MedicalFacilityCategory medicalFacilityCategory;

    @Column(name = "medical_facility_category_id")
    private long medical_facility_category_id;

    @ManyToOne
    @JoinColumn(name = "location_id", insertable = false, updatable = false)
    public Location location;

    @Column(name = "location_id")
    private long location_id;

    @Column(name = "full_address")
    private String full_address;

    @Column(name = "email", length = 100)
    private String email;

    @Column(name = "phone_code", length = 10)
    private String phone_code;

    @Column(name = "phone", length = 15)
    private String phone;

    @Column(name = "fax", length = 15)
    private String fax;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MedicalFacilityCategory getMedicalFacilityCategory() {
        return medicalFacilityCategory;
    }

    public void setMedicalFacilityCategory(MedicalFacilityCategory medicalFacilityCategory) {
        this.medicalFacilityCategory = medicalFacilityCategory;
    }

    public long getMedical_facility_category_id() {
        return medical_facility_category_id;
    }

    public void setMedical_facility_category_id(long medical_facility_category_id) {
        this.medical_facility_category_id = medical_facility_category_id;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public long getLocation_id() {
        return location_id;
    }

    public void setLocation_id(long location_id) {
        this.location_id = location_id;
    }

    public String getFull_address() {
        return full_address;
    }

    public void setFull_address(String full_address) {
        this.full_address = full_address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_code() {
        return phone_code;
    }

    public void setPhone_code(String phone_code) {
        this.phone_code = phone_code;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }
}
