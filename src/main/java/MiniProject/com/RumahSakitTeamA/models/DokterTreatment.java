package MiniProject.com.RumahSakitTeamA.models;

import javax.persistence.*;

@Entity
@Table(name = "t_doctor_treatment")
public class DokterTreatment extends BaseProperties {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long id;

    @ManyToOne
    @JoinColumn(name = "doctor_id", insertable = false, updatable = false)
    public BiodataDokter biodataDokter;

    @Column(name = "doctor_id")
    private long doctor_id;

    @Column(name = "name", length = 50)
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BiodataDokter getBiodataDokter() {
        return biodataDokter;
    }

    public void setBiodataDokter(BiodataDokter biodataDokter) {
        this.biodataDokter = biodataDokter;
    }

    public long getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(long doctor_id) {
        this.doctor_id = doctor_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
