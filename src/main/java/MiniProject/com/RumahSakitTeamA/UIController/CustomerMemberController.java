package MiniProject.com.RumahSakitTeamA.UIController;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("customermember")
public class CustomerMemberController {
    @RequestMapping("")
    public String customerMember() {return "customermember/customermember";}

    @RequestMapping("addcustomermember")
    public String addCustomerMember(){return "customermember/addcustomermember";}

    @RequestMapping("editcustomermember/{id}")
    public String editCustomerMember(@PathVariable("id") Long id, Model model){
        model.addAttribute("id", id);
        return "customermember/editcustomermember";
    }

    @RequestMapping("deletecustomermember/{id}")
    public String deleteCustomerMember(@PathVariable("id") Long id, Model model){
        model.addAttribute("id", id);
        return "customermember/deletecustomermember";
    }
}
