package MiniProject.com.RumahSakitTeamA.UIController;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("customer")
public class CustomerController {
    @RequestMapping("")
    public String customer() {return "customer/customer";}

    @RequestMapping("addcustomer")
    public String addCustomer(){return "customer/addcustomer";}

    @RequestMapping("editcustomer/{id}")
    public String editCustomer(@PathVariable("id") Long id, Model model){
        model.addAttribute("id", id);
        return "customer/editcustomer";
    }

    @RequestMapping("deletecostumer/{id}")
    public String deleteCustomer(@PathVariable("id") Long id, Model model){
        model.addAttribute("id", id);
        return "customer/deletecustomer";
    }
}
