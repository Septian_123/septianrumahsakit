package MiniProject.com.RumahSakitTeamA.UIController;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("nominaldompetelektronik")
public class NominalDompetElektronikController {
    @RequestMapping("")
    public String nominalDompetElektronik(){
        return "nominaldompetelektronik/nominaldompetelektronik";
    }

    @RequestMapping("addnominaldompetelektronik")
    public String addNominalDompetElektronik(){
        return "nominaldompetelektronik/addnominaldompetelektronik";
    }

    @RequestMapping("editnominaldompetelektronik/{id}")
    public String editNominalDompetElektronik(@PathVariable("id") Long id, Model model){
        model.addAttribute("id", id);
        return "nominaldompetelektronik/editnominaldompetelektronik";
    }

    @RequestMapping("deletenominaldompetelektronik/{id}")
    public String deleteNominalDompetElektronik(@PathVariable("id") Long id, Model model){
        model.addAttribute("id", id);
        return "nominaldompetelektronik/deletenominaldompetelektronik";
    }
}
