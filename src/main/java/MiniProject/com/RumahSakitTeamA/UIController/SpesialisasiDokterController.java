package MiniProject.com.RumahSakitTeamA.UIController;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("spesialisasidokter")
public class SpesialisasiDokterController {

    @RequestMapping("")
    public String spesialisasidokter(){ return ("spesialisasidokter/spesialisasidokter"); }

    @RequestMapping("addspesialisasidokter")
    public String addspesialisasidokter(){ return "spesialisasidokter/addSpesialisasiDokter";}

    @RequestMapping("editspesialisasidokter/{id}")
    public String editSpesialisasiDokter(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "spesialisasidokter/editspesialisasidokter";
    }
    @RequestMapping("deletespesialisasidokter/{id}")
    public String deleteSpesialisasiDokter(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "spesialisasidokter/deletespesialisasidokter";
    }
}
