package MiniProject.com.RumahSakitTeamA.UIController;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.ui.Model;

@Controller
@RequestMapping(value = "golongandarah")
public class GolonganDarahController {

    @RequestMapping("")
    public String golongandarah(){
        return "golongandarah/golongandarah";
    }
    @RequestMapping("/addgolongandarah")
    public String addGolonganDarah(){
        return "golongandarah/addgolongandarah";
    }
    @RequestMapping("/editgolongandarah/{id}")
    public String editGolonganDarah(@PathVariable("id") Long id, Model model){
        model.addAttribute("id", id);
        return "golongandarah/editgolongandarah";
    }
    @RequestMapping("/deletegolongandarah/{id}")
    public String deleteGolonganDarah(@PathVariable("id") Long id, Model model){
        model.addAttribute("id", id);
        return "golongandarah/deletegolongandarah";
    }

}
