package MiniProject.com.RumahSakitTeamA.UIController;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("image")
public class ImageController {
    @RequestMapping("")
    public String image() {
        return "image/image";
    }
}
