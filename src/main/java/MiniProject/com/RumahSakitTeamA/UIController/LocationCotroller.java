package MiniProject.com.RumahSakitTeamA.UIController;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("location")
public class LocationCotroller {
    @RequestMapping("")
    public String kota(){ return ("location/location"); }

    @RequestMapping("addlocation")
    public String addlocation(){ return "location/addlocation";}

    @RequestMapping("editlocation/{id}")
    public String editLocation(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "location/editlocation";
    }

    @RequestMapping("deletelocation/{id}")
    public String deleteLocation(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "location/deletelocation";
    }


}
