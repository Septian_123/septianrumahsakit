package MiniProject.com.RumahSakitTeamA.repositories;

import MiniProject.com.RumahSakitTeamA.models.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CustomerRepo extends JpaRepository<Customer, Long> {
    @Query(value = "SELECT * FROM m_customer WHERE is_delete = false", nativeQuery = true)
    List<Customer> findAllNotDeleted();
    @Query(value = "SELECT * FROM m_customer WHERE id = :id", nativeQuery = true)
    Customer findByIdData(long id);
}
