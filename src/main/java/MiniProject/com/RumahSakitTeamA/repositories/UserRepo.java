package MiniProject.com.RumahSakitTeamA.repositories;

import MiniProject.com.RumahSakitTeamA.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserRepo extends JpaRepository<User, Long> {
    @Query(value = "SELECT * FROM public.m_user WHERE lower(m_user.email) = lower(:email)", nativeQuery = true)
    Optional<User> isEmailTerdaftar(String email);

    @Query(value = "SELECT * FROM public.m_user WHERE lower(m_user.email) = lower(:email) AND m_user.password = :password LIMIT 1", nativeQuery = true)
    User login(String email, String password);
}
