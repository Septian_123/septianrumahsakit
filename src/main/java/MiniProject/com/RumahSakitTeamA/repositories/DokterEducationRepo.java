package MiniProject.com.RumahSakitTeamA.repositories;

import MiniProject.com.RumahSakitTeamA.models.Biodata;
import MiniProject.com.RumahSakitTeamA.models.DokterEducation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface DokterEducationRepo extends JpaRepository<DokterEducation, Long> {
    @Query(value = "SELECT * FROM m_doctor_education WHERE id = :id", nativeQuery = true)
    DokterEducation findByIdData(long id);
}
