package MiniProject.com.RumahSakitTeamA.repositories;

import MiniProject.com.RumahSakitTeamA.models.NominalDompetElektronik;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface NominalDompetElektronikRepo extends JpaRepository<NominalDompetElektronik, Long> {
    @Query(value = "SELECT * FROM m_wallet_default_nominal WHERE id = :id", nativeQuery = true)
    NominalDompetElektronik findByIdData(long id);
    @Query(value = "SELECT * FROM m_wallet_default_nominal WHERE is_delete = false", nativeQuery = true)
    List<NominalDompetElektronik> findAllNotDeleted();
    @Query(value = "FROM NominalDompetElektronik WHERE nominal LIKE concat('%',?1,'%') AND is_Delete = false")
    List<NominalDompetElektronik> SearchNominalDompetElektronik(Integer keyword);
}
