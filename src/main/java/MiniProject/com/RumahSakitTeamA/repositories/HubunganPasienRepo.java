package MiniProject.com.RumahSakitTeamA.repositories;

import MiniProject.com.RumahSakitTeamA.models.HubunganPasien;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface HubunganPasienRepo extends JpaRepository<HubunganPasien, Long> {
    @Query(value = "SELECT * FROM m_customer_relation WHERE id = :id", nativeQuery = true)
    HubunganPasien findByIdData(long id);
    @Query(value = "SELECT * FROM m_customer_relation WHERE is_delete = false", nativeQuery = true)
    List<HubunganPasien> findAllNotDeleted();
    @Query(value = "FROM HubunganPasien WHERE lower(name) LIKE lower(concat('%',?1,'%')) AND is_Delete = false")
    List<HubunganPasien> SearchHubunganPasien(String keyword);
}
