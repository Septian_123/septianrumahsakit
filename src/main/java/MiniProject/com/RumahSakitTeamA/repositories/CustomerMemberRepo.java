package MiniProject.com.RumahSakitTeamA.repositories;

import MiniProject.com.RumahSakitTeamA.models.CustomerMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CustomerMemberRepo extends JpaRepository<CustomerMember, Long> {
    @Query(value = "SELECT * FROM m_customer_member WHERE is_delete = false", nativeQuery = true)
    List<CustomerMember> findAllNotDeleted();
    @Query(value = "SELECT * FROM m_customer_member WHERE parent_biodata_id = :id",nativeQuery = true)
    List<CustomerMember> findAllCustomerMemberByParentId(Long id);
    @Query(value = "SELECT * FROM m_customer_member WHERE id = :id", nativeQuery = true)
    CustomerMember findByIdData(Long id);
}
