package MiniProject.com.RumahSakitTeamA.repositories;

import MiniProject.com.RumahSakitTeamA.models.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RoleRepo extends JpaRepository<Role, Long> {
    @Query(value = "SELECT * FROM m_role ORDER BY m_role ASC", nativeQuery = true)
    List<Role> findAllRoleOrderbyASC();
    @Query(value = "SELECT * FROM m_role WHERE is_delete = false ORDER BY m_role.id ASC",nativeQuery = true)
    Page<Role> findAllOrderByAsc(Pageable pageable);

    @Query(value = "SELECT * FROM m_role",nativeQuery = true)
    Page<Role> findAllOrder(Pageable pageable);

    @Query(value = "SELECT * FROM m_role WHERE lower(m_role.name) LIKE lower(concat('%',:kata,'%'))", nativeQuery = true)
    List<Role> search(String kata);

    @Query(value = "SELECT * FROM m_role WHERE id = :id", nativeQuery = true)
    Role findByIdData(long id);

    @Query(value = "SELECT * FROM m_role WHERE is_delete = false ORDER BY m_role.id ASC", nativeQuery = true)
    List<Role> findAllNotDeleted();

    @Query(value = "SELECT * FROM m_role WHERE lower(m_role.name) LIKE lower (concat('%',:kata,'%') )", nativeQuery = true)
    Page<Role> searchMapped(Pageable pageable, String kata);
}
