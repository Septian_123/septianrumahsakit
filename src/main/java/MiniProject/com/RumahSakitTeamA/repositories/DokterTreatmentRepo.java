package MiniProject.com.RumahSakitTeamA.repositories;

import MiniProject.com.RumahSakitTeamA.models.Biodata;
import MiniProject.com.RumahSakitTeamA.models.DokterTreatment;
import MiniProject.com.RumahSakitTeamA.models.ProfilDokterShare;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface DokterTreatmentRepo extends JpaRepository<DokterTreatment, Long> {
    @Query(value = "SELECT * FROM t_doctor_treatment WHERE id = :id", nativeQuery = true)
    DokterTreatment findByIdData (Long id);

    @Query(value = "SELECT * FROM t_doctor_treatment WHERE id = :id and is_delete = false", nativeQuery = true)
    Optional<DokterTreatment> findProfil(Long id);

    @Query(value = "SELECT * FROM t_doctor_treatment WHERE t_doctor_treatment.doctor_id = :id AND is_delete = false ", nativeQuery = true)
    List<DokterTreatment> FindBYDoctorID(Long id);

//    @Query(value = "SELECT * FROM t_doctor_treatment WHERE  is_delete = false", nativeQuery = true)
//    DokterTreatment findNotDeletedByID(Long id);
}
