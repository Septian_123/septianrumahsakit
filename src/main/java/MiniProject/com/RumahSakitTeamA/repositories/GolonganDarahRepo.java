package MiniProject.com.RumahSakitTeamA.repositories;

import MiniProject.com.RumahSakitTeamA.models.GolonganDarah;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface GolonganDarahRepo extends JpaRepository<GolonganDarah, Long> {
    @Query(value = "SELECT * FROM m_blood_group WHERE id = :id", nativeQuery = true)
    GolonganDarah findByIdData(long id);
    @Query(value = "SELECT * FROM m_blood_group WHERE is_delete = false", nativeQuery = true)
    List<GolonganDarah> findAllNotDeleted();
    @Query(value = "FROM GolonganDarah WHERE lower(code) LIKE lower(concat('%',?1,'%')) AND is_Delete = false")
    List<GolonganDarah> SearchGolonganDarah(String keyword);
}