package MiniProject.com.RumahSakitTeamA.repositories;

import MiniProject.com.RumahSakitTeamA.models.Biodata;
import MiniProject.com.RumahSakitTeamA.models.MedicalFacilityCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface MedicalFacilityCategoryRepo extends JpaRepository<MedicalFacilityCategory, Long> {
    @Query(value = "SELECT * FROM m_medical_facility_category WHERE id = :id", nativeQuery = true)
    MedicalFacilityCategory findByIdData(long id);
}
