package MiniProject.com.RumahSakitTeamA.controllers;

import MiniProject.com.RumahSakitTeamA.models.Biodata;
import MiniProject.com.RumahSakitTeamA.models.BiodataDokter;
import MiniProject.com.RumahSakitTeamA.repositories.BiodataDokterRepo;
import MiniProject.com.RumahSakitTeamA.repositories.BiodataRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiBiodataDokterController {
    @Autowired
    private BiodataDokterRepo biodataDokterRepo;

    @GetMapping("/getallbiodatadokter")
    public ResponseEntity<List<BiodataDokter>> GetAllBiodataDokter()
    {
        try {
            List<BiodataDokter> biodatadokter = this.biodataDokterRepo.findAll();
            return new ResponseEntity<>(biodatadokter, HttpStatus.OK);
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/getbyidbiodatadokter/{id}")
    public ResponseEntity<List<BiodataDokter>> GetBiodataDokterById(@PathVariable("id") Long id)
    {
        try {
            Optional<BiodataDokter> biodatadokter = this.biodataDokterRepo.findById(id);

            if (biodatadokter.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(biodatadokter, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/addbiodatadokter")
    public ResponseEntity<Object> SaveBiodataDokter(@RequestBody BiodataDokter biodatadokter)
    {
        try {
            biodatadokter.setCreated_By(1L);
            biodatadokter.setCreated_On(new Date());
            this.biodataDokterRepo.save(biodatadokter);
            return new ResponseEntity<>(biodatadokter, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("editbiodatadokter/{id}")
    public ResponseEntity<Object> EditBiodataDokter(@RequestBody BiodataDokter biodatadokter, @PathVariable("id") Long id)
    {
        Optional<BiodataDokter> biodatadokterData = this.biodataDokterRepo.findById(id);

        if (biodatadokterData.isPresent())
        {
            biodatadokter.setId(id);
            biodatadokter.setModified_By(1L);
            biodatadokter.setModified_On(new Date());
            this.biodataDokterRepo.save(biodatadokter);
            ResponseEntity rest =new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }
        else
        {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("deletebiodatadokter/{id}")
    public String DeleteBiodataDokter(@PathVariable("id") Long id)
    {
        try {
            BiodataDokter biodatadokter = this.biodataDokterRepo.findByIdData(id);
            biodatadokter.setDeleted_By(1L);
            biodatadokter.setDeleted_On(new Date());
            biodatadokter.setIs_Delete(true);
            this.biodataDokterRepo.save(biodatadokter);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }
}
