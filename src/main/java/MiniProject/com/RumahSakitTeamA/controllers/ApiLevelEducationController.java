package MiniProject.com.RumahSakitTeamA.controllers;


import MiniProject.com.RumahSakitTeamA.models.LevelEducation;
import MiniProject.com.RumahSakitTeamA.repositories.LevelEducationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiLevelEducationController {
    @Autowired
    private LevelEducationRepo levelEducationRepo;

    @GetMapping("/getallleveleducation")
    public ResponseEntity<List<LevelEducation>> GetAllLevelEducation()
    {
        try {
            List<LevelEducation> leveleducation = this.levelEducationRepo.findAll();
            return new ResponseEntity<>(leveleducation, HttpStatus.OK);
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/getbyidleveleducation/{id}")
    public ResponseEntity<List<LevelEducation>> GetleveleducationById(@PathVariable("id") Long id)
    {
        try {
            Optional<LevelEducation> leveleducation = this.levelEducationRepo.findById(id);

            if (leveleducation.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(leveleducation, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/addleveleducation")
    public ResponseEntity<Object> Saveleveleducation(@RequestBody LevelEducation leveleducation)
    {
        try {
            leveleducation.setCreated_By(1L);
            leveleducation.setCreated_On(new Date());
            this.levelEducationRepo.save(leveleducation);
            return new ResponseEntity<>(leveleducation, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("editleveleducation/{id}")
    public ResponseEntity<Object> Editleveleducation(@RequestBody LevelEducation leveleducation, @PathVariable("id") Long id)
    {
        Optional<LevelEducation> leveleducationData = this.levelEducationRepo.findById(id);

        if (leveleducationData.isPresent())
        {
            leveleducation.setId(id);
            leveleducation.setModified_By(1L);
            leveleducation.setModified_On(new Date());
            this.levelEducationRepo.save(leveleducation);
            ResponseEntity rest =new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }
        else
        {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("deleteleveleducation/{id}")
    public String Deleteleveleducation(@PathVariable("id") Long id)
    {
        try {
            LevelEducation leveleducation = this.levelEducationRepo.findByIdData(id);
            leveleducation.setDeleted_By(1L);
            leveleducation.setDeleted_On(new Date());
            leveleducation.setIs_Delete(true);
            this.levelEducationRepo.save(leveleducation);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }
}
