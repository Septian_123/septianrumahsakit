package MiniProject.com.RumahSakitTeamA.controllers;

import MiniProject.com.RumahSakitTeamA.models.Biodata;
import MiniProject.com.RumahSakitTeamA.models.Customer;
import MiniProject.com.RumahSakitTeamA.models.CustomerMember;
import MiniProject.com.RumahSakitTeamA.repositories.BiodataRepo;
import MiniProject.com.RumahSakitTeamA.repositories.CustomerMemberRepo;
import MiniProject.com.RumahSakitTeamA.repositories.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiCustomerMemberController {

    @Autowired
    private CustomerMemberRepo customerMemberRepo;
    @Autowired
    private BiodataRepo biodataRepo;
    @Autowired
    private CustomerRepo customerRepo;


    @GetMapping("/getallcustomermember")
    public ResponseEntity<List<CustomerMember>> GetAllCustomerMember()
    {
        try {
            List<CustomerMember> customerMember = this.customerMemberRepo.findAllNotDeleted();
            return new ResponseEntity<>(customerMember, HttpStatus.OK);
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
    @GetMapping("/getbyparentidcustomermember/{id}")
    public ResponseEntity<List<CustomerMember>> GetCustomerMemberByParentId(@PathVariable("id") Long id)
    {
        try {
            List<CustomerMember> customerMember = this.customerMemberRepo.findAllCustomerMemberByParentId(id);
            return new ResponseEntity<>(customerMember,HttpStatus.OK);
        }
        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/addcustomermember")
    public ResponseEntity<Object> SaveCustomerMember(@RequestBody CustomerMember customerMember)
    {
        try {
            customerMember.setCreated_By(1L);
            customerMember.setCreated_On(new Date());
            this.customerMemberRepo.save(customerMember);
            return new ResponseEntity<>(customerMember, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getbyidcustomermember/{id}")
    public ResponseEntity<CustomerMember> GetCustomerMemberById(@PathVariable("id") Long id)
    {
        try {
            Optional<CustomerMember> customerMember = this.customerMemberRepo.findById(id);

            if (customerMember.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(customerMember, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/editcustomermember/{id}")
    public ResponseEntity<Object> EditCustomerMember(@RequestBody CustomerMember customerMember, @PathVariable("id") Long id)
    {
        Optional<CustomerMember> customerMemberData = this.customerMemberRepo.findById(id);

        if (customerMemberData.isPresent())
        {
            customerMember.setId(id);
            customerMember.setModified_By(1L);
            customerMember.setModified_On(new Date());
            this.customerMemberRepo.save(customerMember);
            ResponseEntity rest =new ResponseEntity<>(customerMember, HttpStatus.OK);
            return rest;
        }
        else
        {
            return ResponseEntity.notFound().build();
        }
    }
    @DeleteMapping("/deletecustomermember/{id}")
    public String DeleteCustomerMember(@PathVariable("id") Long id)
    {
        try {
            CustomerMember customerMember = this.customerMemberRepo.findByIdData(id);
            customerMember.setDeleted_By(1L);
            customerMember.setDeleted_On(new Date());
            customerMember.setIs_Delete(true);
            this.customerMemberRepo.save(customerMember);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }

    @DeleteMapping("/deletemultiplecustomermember")
    public String DeleteMultipleToGetId(@RequestBody Long[] ids)
    {
        try {
//             buat biodata
            for (int i = 0; i < ids.length; i++) {
                CustomerMember customerMemberBiodata = this.customerMemberRepo.findByIdData(ids[i]);
                long idCoba = customerMemberBiodata.customer.getBiodata_id();
                Biodata biodata = this.biodataRepo.findByIdData(customerMemberBiodata.customer.getBiodata_id());
                biodata.setDeleted_By(1L);
                biodata.setDeleted_On(new Date());
                biodata.setIs_Delete(true);
                this.biodataRepo.save(biodata);
            }
//             buat customer
                for (int i = 0; i < ids.length; i++) {
                CustomerMember customerMemberCustomer = this.customerMemberRepo.findByIdData(ids[i]);
                Customer customer = this.customerRepo.findByIdData(customerMemberCustomer.customer.getId());
                customer.setDeleted_By(1L);
                customer.setDeleted_On(new Date());
                customer.setIs_Delete(true);
                this.customerRepo.save(customer);
            }

            for (int i = 0; i < ids.length; i++) {
                long id = ids [i];
                CustomerMember customerMember = this.customerMemberRepo.findByIdData(id);
                customerMember.setDeleted_By(1L);
                customerMember.setDeleted_On(new Date());
                customerMember.setIs_Delete(true);
                this.customerMemberRepo.save(customerMember);
            }
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }
}